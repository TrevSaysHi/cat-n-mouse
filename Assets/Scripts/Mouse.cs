﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mouse : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2 }

	Actions[] baseActions = { Actions.Idle, Actions.MoveLeft, Actions.MoveRight };

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 13;
	const int DEFAULT_ROOM_LAYER = 8;

	static Vector3 insideRoom = new Vector3 (0f, 4.3f, 0f);
	static Vector3 outsideRoom = new Vector3 (-8f, 4.3f, 0f);
	static Vector3 behindDoor = new Vector3 (0f, -17.85f, 0f);
	static Vector3 inMailRoom = new Vector3 (-11.5f, -15.91f, 0f);
	static Vector3 outOfView = new Vector3 (-37f, -17.85f, 0f);
	static Vector3 inFrontOfFlowers = new Vector3 (-13.28f, -17.85f, 0f);
	static Vector3 inFrontOfBike = new Vector3 (18.75f, -17.85f, 0f);
	static Vector3 outsideRoomBalcony = new Vector3 (18.75f, 4.52f, 0f);

	Transitions[] toMailRoom = {
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 1),
		new Transitions (behindDoor, 10f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER, 0),
		new Transitions (inMailRoom, 5f, DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toOffScreen = {
		new Transitions (behindDoor, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER, 1),
		new Transitions (outOfView, 10f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER + 5, DEFAULT_ROOM_LAYER, 1),
	};
	Transitions[] toInFrontOfFlowers = {
		new Transitions (inFrontOfFlowers, 10f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER + 5, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toRoomFromFlowers = {
		new Transitions (inFrontOfBike, 10f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER + 5, DEFAULT_ROOM_LAYER, 4),
		new Transitions (outsideRoomBalcony, 10f, DEFAULT_CHARACTER_LAYER - 5, DEFAULT_ROOM_LAYER - 5, 5),
		new Transitions (insideRoom, 10f, DEFAULT_CHARACTER_LAYER - 5, DEFAULT_ROOM_LAYER - 5, 4)
	};

	Animator animator, roomAnimator;
	SpriteRenderer spriteRenderer, roomRenderer, bushRenderer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public Sprite badBush;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float speed;
	public int start = 0;
	public int leaveToMailRoom = 20;
	public int readsMail = 40;
	public int argue = 60;
	public int leaveToOutsideFromMail = 80;
	public int comesBackFromOffScreen = 110;
	public int fucksUpFlowers = 120;
	public int goesToRoomFromOutsideUsingFireEscape = 125;
	// public int pullsUpFireEscapeUp = 135;
	// public int entersRoomFromOutside = 140;
	// public int putsUpFlowers = 145;
	public int startsBumpin = 150;
	// private bool canAnimate = true;
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";

	private bool canStartCoRoutine = true;
	private bool canAnimate = true;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		roomAnimator = GameObject.Find ("Mouses Room").GetComponent<Animator> ();
		roomRenderer = GameObject.Find ("Mouses Room").GetComponent<SpriteRenderer> ();
		bushRenderer = GameObject.Find ("Left Bush").GetComponent<SpriteRenderer> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();

		setLayers (DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER);

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}

	// Update is called once per frame
	void Update () {
		if (CheckIfInTime (start, leaveToMailRoom - 1)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds (leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime (leaveToMailRoom, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toMailRoom);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime (readsMail, argue)) {
			animator.SetInteger ("State", 2);
		}
		if (CheckIfInTime (argue, leaveToOutsideFromMail)) {
			animator.SetInteger ("State", 3);
			SetXScale (.5f);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime (leaveToOutsideFromMail, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toOffScreen);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime (leaveToOutsideFromMail, comesBackFromOffScreen)) {
			canStartCoRoutine = true;
		}
		if (CheckIfInTime (comesBackFromOffScreen, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toInFrontOfFlowers);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime (fucksUpFlowers, 0)) {
			animator.SetInteger ("State", 6);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime (goesToRoomFromOutsideUsingFireEscape, 0)) {
			if (canStartCoRoutine) {
				bushRenderer.sprite = badBush;
				roomTransition = RoomTransition (toRoomFromFlowers);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime (startsBumpin, 0)) {
			setLayers (DEFAULT_ROOM_LAYER, DEFAULT_CHARACTER_LAYER);
			roomAnimator.SetInteger("State", 1);
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if (location.Equals (LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			}
		}
		SetAction ();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int) actionList[nextAction];

		Debug.Log ("Current Action for " + this.gameObject.name + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction () {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		} else if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			SetXScale (-.5f);
		} else if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			SetXScale (.5f);
		}
	}

	void CheckIfOutOfBounds (float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
			SetAction ();
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
			SetAction ();
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	void setLayers (int newCharLayer, int newRoomRenderer) {
		spriteRenderer.sortingOrder = newCharLayer;
		roomRenderer.sortingOrder = newRoomRenderer;
	}

	private IEnumerator RoomTransition (Transitions[] positionsToMoveTo) {
		float elapsedTime;
		Vector3 startPos;

		for (int i = 0; i < positionsToMoveTo.Length; i++) {
			elapsedTime = 0f;
			startPos = transform.position;

			if (transform.position.x < positionsToMoveTo[i].position.x) {
				SetXScale (.5f);
			} else {
				SetXScale (-.5f);
			}

			setLayers (positionsToMoveTo[i].characterLayer, positionsToMoveTo[i].roomLayer);
			animator.SetInteger ("State", positionsToMoveTo[i].characterAnimationState);

			while (elapsedTime < positionsToMoveTo[i].transitionTime) {
				transform.position = Vector3.Lerp (startPos, positionsToMoveTo[i].position, elapsedTime / positionsToMoveTo[i].transitionTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}