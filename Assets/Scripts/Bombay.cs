﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bombay : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2, ThrowBall = 3, Argue = 4 }
	Actions[] baseActions = { Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.ThrowBall };
	Actions[] actionsOutside = { Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.ThrowBall };

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 12;
	const int DEFAULT_ROOM_LAYER = 8;

	static Vector3 outsideRoom = new Vector3 (3f, 12.84f, 0f);
	static Vector3 insideRoom = new Vector3 (10f, 12.84f, 0f);
	static Vector3 behindDoor = new Vector3 (0f, -17.3f, 0f);
	static Vector3 inMailRoom = new Vector3 (-7.14f, -16.37f, 0f);
	static Vector3 outOfView = new Vector3 (37f, -17.3f, 0f);

	Transitions[] toMailFromRoom = {
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 1),
		new Transitions (behindDoor, 5f, DEFAULT_CHARACTER_LAYER - 4, DEFAULT_ROOM_LAYER - 4, 0),
		new Transitions (inMailRoom, 5f, DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toRoomFromMail = {
		new Transitions (behindDoor, 5f, DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1),
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 0),
		new Transitions (insideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 1)
	};
	Transitions[] toOutsideFromRoom = {
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 5),
		new Transitions (behindDoor, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 0),
		new Transitions (outOfView, 15f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 5)
	};
	Transitions[] toRoomFromOutside = {
		new Transitions (behindDoor, 15f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER - 2, 5),
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 0),
		new Transitions (insideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 5)
	};

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float speed;
	public int start = 0;
	public int leaveRoom = 40;
	public int stareAtMouse = 55;
	public int argue = 60;
	public int goToRoom = 85;
	public int leavesToGoOutside = 165;
	public int comesBackFromOffScreen = 200;
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";
	const string LOCATION_OUTSIDE = "outside";

	private bool goToMailRoomFromRoomCoroutineOnce = true;
	private bool goToRoomFromMailRoomCoroutineOnce = true;
	private bool goToOutsideFromRoomCoroutineOnce = true;
	private bool goToRoomFromOutsideCoroutineOnce = true;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		roomLayer = GameObject.Find ("Bombays Room").GetComponent<SpriteRenderer> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();

		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}

	// Update is called once per frame
	void Update () {
		if (CheckIfInTime (start, leaveRoom)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds (leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime (leaveRoom, 0)) {
			if (goToMailRoomFromRoomCoroutineOnce) {
				roomTransition = RoomTransition (toMailFromRoom);
				StartCoroutine (roomTransition);
				goToMailRoomFromRoomCoroutineOnce = false;
			}
		}
		if(CheckIfInTime (stareAtMouse, argue)) {
			animator.SetInteger ("State", 0);
		}
		if(CheckIfInTime (argue, goToRoom)) {
			animator.SetInteger ("State", 3);
		}
		if (CheckIfInTime (goToRoom, 0)) {
			if (goToRoomFromMailRoomCoroutineOnce) {
				roomTransition = RoomTransition (toRoomFromMail);
				StartCoroutine (roomTransition);
				goToRoomFromMailRoomCoroutineOnce = false;
			}
		}
		if (CheckIfInTime(goToRoom + 15, leavesToGoOutside)) {
			animator.SetInteger ("State", 4);
			transform.position = new Vector3(10f, 12.57f, transform.position.z);
		}
		if (CheckIfInTime (leavesToGoOutside, 0)) {
			if (goToOutsideFromRoomCoroutineOnce) {
				roomTransition = RoomTransition (toOutsideFromRoom);
				StartCoroutine (roomTransition);
				goToOutsideFromRoomCoroutineOnce = false;
			}
		}
		if (CheckIfInTime (leavesToGoOutside, 0)) {
			if (goToOutsideFromRoomCoroutineOnce) {
				roomTransition = RoomTransition (toOutsideFromRoom);
				StartCoroutine (roomTransition);
				goToOutsideFromRoomCoroutineOnce = false;
			}
		}
		if (CheckIfInTime (comesBackFromOffScreen, 0)) {
			if (goToRoomFromOutsideCoroutineOnce) {
				roomTransition = RoomTransition (toRoomFromOutside);
				StartCoroutine (roomTransition);
				goToRoomFromOutsideCoroutineOnce = false;
			}
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if (location.Equals (LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			} else if (location.Equals (LOCATION_OUTSIDE)) {
				ChooseNextAction (actionsOutside, 2.0f, 10.0f);
			}
		}
		SetAction ();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int) actionList[nextAction];

		// Debug.Log ("Current Action for " + this.gameObject.name + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction () {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		}
		if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (-.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (-.5f);
		}
		if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			// transform.localScale = new Vector3 (.5f, transform.localScale.y, transform.localScale.z);
			SetXScale (.5f);
		}
		if (currentAction == (int) Actions.ThrowBall) {
			animator.SetInteger ("State", 2);
		}
	}

	void CheckIfOutOfBounds (float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	void setLayers (int newCharLayer, int newRoomLayer) {
		spriteRenderer.sortingOrder = newCharLayer;
		roomLayer.sortingOrder = newRoomLayer;
	}

	private IEnumerator RoomTransition (Transitions[] positionsToMoveTo) {
		float elapsedTime;
		Vector3 startPos;

		for (int i = 0; i < positionsToMoveTo.Length; i++) {
			elapsedTime = 0f;
			startPos = transform.position;

			if (transform.position.x < positionsToMoveTo[i].position.x) {
				SetXScale (.5f);
			} else {
				SetXScale (-.5f);
			}

			setLayers (positionsToMoveTo[i].characterLayer, positionsToMoveTo[i].roomLayer);
			animator.SetInteger ("State", positionsToMoveTo[i].characterAnimationState);

			while (elapsedTime < positionsToMoveTo[i].transitionTime) {
				transform.position = Vector3.Lerp (startPos, positionsToMoveTo[i].position, elapsedTime / positionsToMoveTo[i].transitionTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}