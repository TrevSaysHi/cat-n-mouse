﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingahPurrah : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2, Sing1 = 3, Sing2 = 4, Interrupted = 5, Annoyed = 6 }
	Actions[] actionsInside = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.Sing1, Actions.Sing2};
	Actions[] actionsOutside = {Actions.Idle, Actions.Sing1, Actions.Sing2};
	Actions[] actionsInsideAngry = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.Interrupted, Actions.Annoyed};

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 12;
	const int DEFAULT_ROOM_LAYER = 8;

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float speed;
	public int start = 0;
	public int movesToBalcony = 60;
	public int goesInside = 150;
	public int end = 300;
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";
	const string LOCATION_INSIDE_ANGRY = "insideRoomAngry";
	const string LOCATION_OUTSIDE = "outside";

	private bool goOutsideToSingCoRoutine = true;
	private bool goInsideToSingCoRoutine = true;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
		roomLayer = GameObject.Find ("SingaPurrahs Room").GetComponent<SpriteRenderer> ();

		spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER;
		roomLayer.sortingOrder = DEFAULT_ROOM_LAYER;

		ChooseNextAction (actionsInside, 2.0f, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		// Debug.Log(timeController.timeInSeconds);
		// Debug.Log(canAnimate);
		if(CheckIfInTime(start, movesToBalcony)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime (movesToBalcony, 0)) {
			if (goOutsideToSingCoRoutine) {
				// spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER - 2;
				// roomLayer.sortingOrder = DEFAULT_ROOM_LAYER - 2;
				animator.SetInteger ("State", 1);
				roomTransition = RoomTransition(transform.position, new Vector3 (-17.13f, -4.32f, transform.position.z));
				StartCoroutine (roomTransition);
				goOutsideToSingCoRoutine = false;
			}
		}
		if(CheckIfInTime(movesToBalcony + 5, goesInside)) {
			DetermineActionFromLocation (LOCATION_OUTSIDE);
		}
		if(CheckIfInTime(goesInside, 0)) {
			if (goInsideToSingCoRoutine) {
				// spriteRenderer.sortingOrder = DEFAULT_CHARACTER_LAYER - 2;
				// roomLayer.sortingOrder = DEFAULT_ROOM_LAYER - 2;
				animator.SetInteger ("State", 1);
				roomTransition = RoomTransition(transform.position, new Vector3 (-10f, -3.61f, transform.position.z));
				StartCoroutine (roomTransition);
				goInsideToSingCoRoutine = false;
			}
		}
		if(CheckIfInTime(goesInside + 5, end)) {
			DetermineActionFromLocation (LOCATION_INSIDE_ANGRY);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		// Debug.Log("currentAction: " + currentAction);
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if(location.Equals(LOCATION_INSIDE)) {
				ChooseNextAction (actionsInside, 2.0f, 4.0f);
			} else if(location.Equals(LOCATION_OUTSIDE)) {
				Debug.Log("outside action");
				ChooseNextAction (actionsOutside, 2.0f, 10.0f);
			} else if (location.Equals(LOCATION_INSIDE_ANGRY)) {
				ChooseNextAction (actionsInsideAngry, 2.0f, 10.0f);
			}
	 	}
		SetAction();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int)actionList[nextAction];

		// Debug.Log("Current Action for " + this.gameObject.name  + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction () {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		}
		if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			SetXScale (-.5f);
		}
		if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			SetXScale (.5f);
		}
		if (currentAction == (int) Actions.Sing1) {
			animator.SetInteger ("State", 2);
		}
		if (currentAction == (int) Actions.Sing2) {
			animator.SetInteger ("State", 3);
		}
		if (currentAction == (int) Actions.Interrupted) {
			animator.SetInteger ("State", 4);
		}
		if (currentAction == (int) Actions.Annoyed) {
			animator.SetInteger ("State", 5);
		}
	}

	void CheckIfOutOfBounds(float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	private IEnumerator RoomTransition (Vector3 startPos, Vector3 endPos) {
		float elapsedTime = 0f;
		float secondsToTransitionToRoom = 5.0f;

		if(startPos.x < endPos.x) {
			SetXScale(.5f);
		} else {
			SetXScale(-.5f);
		}

		while (elapsedTime < secondsToTransitionToRoom) {
			transform.position = Vector3.Lerp (startPos, endPos, elapsedTime / secondsToTransitionToRoom);
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}