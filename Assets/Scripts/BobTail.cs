﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BobTail : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2, WaterBush = 3 }
	Actions[] baseActions = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight};
	Actions[] actionsOutside = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight};
	Actions[] wateringBushes = {Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.WaterBush};

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 13;
	const int DEFAULT_ROOM_LAYER = 8;

	static Vector3 outsideRoom = new Vector3 (3f, -3.61f, 0f);
	static Vector3 insideRoom = new Vector3 (10f, -3.61f, 0f);
	static Vector3 behindDoor = new Vector3 (0f, -17.5f, 0f);
	static Vector3 inFrontOfLeftBush = new Vector3 (-16f, -17.59f, 0f);
	static Vector3 inFrontOfRightBush = new Vector3 (16f, -17.59f, 0f);

	Transitions[] toRightBushFromInside = {
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 1),
		new Transitions (behindDoor, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 0),
		new Transitions (inFrontOfRightBush, 5f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toLeftBush = {
		new Transitions (inFrontOfLeftBush, 10f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toRightBush = {
		new Transitions (inFrontOfRightBush, 10f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER, 1)
	};
	Transitions[] toInside = {
		new Transitions (behindDoor, 15f, IN_FRONT_OF_BUILDING_CHARACTER_LAYER, DEFAULT_ROOM_LAYER - 2, 1),
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 1),
		new Transitions (insideRoom, 5f, DEFAULT_CHARACTER_LAYER - 2, DEFAULT_ROOM_LAYER - 2, 1)
	};

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float leftBushLeftBoundary;
	public float leftBushRightBoundary;
	public float rightBushLeftBoundary;
	public float rightBushRightBoundary;
	public float speed;
	public int start = 0;
	public int leaveRoom = 30;
	public int waterRightBush1 = 45;
	public int waterLeftBush1 = 75;
	public int waterRightBush2 = 100;
	public int getMad = 125;
	public int goToRoom = 150;
	public int idleInRoom = 175;
	public int sleep = 250;
	public int end = 300;
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";
	const string LOCATION_OUTSIDE = "outside";
	const string LOCATION_BUSH = "bushes";

	private bool canStartCoRoutine = true;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		roomLayer = GameObject.Find ("BobTails Room").GetComponent<SpriteRenderer> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();

		setLayers(DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER);

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}
	
	// Update is called once per frame
	void Update () {
		if(CheckIfInTime(start, leaveRoom)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime(leaveRoom, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toRightBushFromInside);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime(waterRightBush1, waterLeftBush1 - 1)) {
			DetermineActionFromLocation(LOCATION_BUSH);
			CheckIfOutOfBounds(rightBushLeftBoundary, rightBushRightBoundary);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime(waterLeftBush1, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toLeftBush);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime(waterLeftBush1, waterRightBush2 - 1)) {
			DetermineActionFromLocation(LOCATION_BUSH);
			CheckIfOutOfBounds(leftBushLeftBoundary, leftBushRightBoundary);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime(waterRightBush2, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toRightBush);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime(waterRightBush2, getMad - 1)) {
			DetermineActionFromLocation(LOCATION_BUSH);
			CheckIfOutOfBounds(rightBushLeftBoundary, rightBushRightBoundary);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime(getMad, goToRoom - 1)) {
			animator.SetInteger ("State", 5);
			SetXScale (-.5f);
		} 
		if (CheckIfInTime(goToRoom, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toInside);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime(idleInRoom, sleep)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds(leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime(sleep, end)) {
			animator.SetInteger ("State", 4);
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if(location.Equals(LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			} else if(location.Equals(LOCATION_OUTSIDE)) {
				ChooseNextAction (actionsOutside, 2.0f, 10.0f);
			} else if(location.Equals(LOCATION_BUSH)) {
				ChooseNextAction (wateringBushes, 2.0f, 5.0f);
			}
	 	}
		SetAction();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int)actionList[nextAction];

		// Debug.Log("Current Action for " + this.gameObject.name  + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction() {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		}else if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			SetXScale (-.5f);
		} else if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			SetXScale (.5f);
		} else if (currentAction == (int) Actions.WaterBush) {
			animator.SetInteger ("State", 3);
		}
	}

	void CheckIfOutOfBounds(float leftBoundary, float rightBoundary) {
		if (transform.position.x < leftBoundary) {
			currentAction = (int) Actions.MoveRight;
		}
		if (transform.position.x > rightBoundary) {
			currentAction = (int) Actions.MoveLeft;
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	void setLayers (int newCharLayer, int newRoomLayer) {
		spriteRenderer.sortingOrder = newCharLayer;
		roomLayer.sortingOrder = newRoomLayer;
	}

	private IEnumerator RoomTransition (Transitions[] positionsToMoveTo) {
		float elapsedTime;
		Vector3 startPos;

		for (int i = 0; i < positionsToMoveTo.Length; i++) {
			elapsedTime = 0f;
			startPos = transform.position;

			if (transform.position.x < positionsToMoveTo[i].position.x) {
				SetXScale (.5f);
			} else {
				SetXScale (-.5f);
			}

			setLayers (positionsToMoveTo[i].characterLayer, positionsToMoveTo[i].roomLayer);
			animator.SetInteger ("State", positionsToMoveTo[i].characterAnimationState);

			while (elapsedTime < positionsToMoveTo[i].transitionTime) {
				transform.position = Vector3.Lerp (startPos, positionsToMoveTo[i].position, elapsedTime / positionsToMoveTo[i].transitionTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}
