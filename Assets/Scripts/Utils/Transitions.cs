﻿using UnityEngine;

public class Transitions {
	public Vector3 position;
	public float transitionTime;
	public int characterLayer;
	public int roomLayer;
	public int characterAnimationState;

	public Transitions (Vector3 positionToTransformTo, float timeToTransition) {
		position = positionToTransformTo;
		transitionTime = timeToTransition;
	}

	public Transitions (Vector3 positionToTransformTo, float timeToTransition, int characterLayerToChangeTo, int roomLayerToChangeTo, int newCharacterAnimationState) {
		position = positionToTransformTo;
		transitionTime = timeToTransition;
		characterLayer = characterLayerToChangeTo;
		roomLayer = roomLayerToChangeTo;
		characterAnimationState = newCharacterAnimationState;
	}
}