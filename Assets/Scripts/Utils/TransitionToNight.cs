﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionToNight : MonoBehaviour {

	SpriteRenderer spriteRenderer;
	TimeController timeController;

	private IEnumerator fadeOutRoutine;

	public float toValue;
	public float fromValue;

	private bool startCoroutine = true;

	// Use this for initialization
	void Start () {
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();
	}

	// Update is called once per frame
	void Update () {
		if (timeController.timeInSeconds == 150 && startCoroutine) {
			fadeOutRoutine = FadeOut(10f);
			StartCoroutine(fadeOutRoutine);
			startCoroutine = false;
		}
	}

	IEnumerator FadeOut (float time) {
		float elapsedTime = 0f;

		while(elapsedTime < time) {
			spriteRenderer.color = new Color(1f,1f,1f,Mathf.SmoothStep(fromValue, toValue, elapsedTime / time));
			elapsedTime += Time.deltaTime;
			yield return null;
		}

		StopCoroutine(fadeOutRoutine);
	}
}