﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {
	public float speed = 3f;
	private Vector3 movement;
	private float xmin;
	private float xmax;
	private float ymin;
	private float ymax;
	private float vertExtent;
	private float horzExtent;

	// Use this for initialization
	void Start () {
		vertExtent = GetComponent<Camera>().orthographicSize;
		horzExtent = vertExtent * Screen.width / Screen.height;

		xmin = GameObject.Find("Left Border").GetComponent<Transform>().transform.position.x + horzExtent;
		xmax = GameObject.Find("Right Border").GetComponent<Transform>().transform.position.x - horzExtent;
		ymin = GameObject.Find("Bottom Border").GetComponent<Transform>().transform.position.y + vertExtent;
		ymax = GameObject.Find("Top Border").GetComponent<Transform>().transform.position.y - vertExtent;

		movement.x = Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed;
		movement.y = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed;

		movement.z = -10f;

		vertExtent = GetComponent<Camera>().orthographicSize;
		horzExtent = vertExtent * Screen.width / Screen.height;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetAxisRaw("Mouse X") != 0f || Input.GetAxisRaw("Mouse Y") != 0f) {
			movement.x = Input.GetAxisRaw("Mouse X") * Time.deltaTime * speed;
			movement.y = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * speed;
		} else if(Input.GetAxisRaw("Horizontal") != 0f || Input.GetAxisRaw("Vertical") != 0f) {
			movement.x = Input.GetAxis ("Horizontal") * Time.deltaTime * speed;
			movement.y = Input.GetAxis ("Vertical") * Time.deltaTime * speed;
		} else {
			movement.x = 0f;
			movement.y = 0f;
		}

		transform.position += movement;
	}

	void LateUpdate () {
		float xClamp = Mathf.Clamp(transform.position.x, xmin, xmax);
		float yClamp = Mathf.Clamp(transform.position.y, ymin, ymax);
		transform.position = new Vector3(xClamp, yClamp, -10f);
	}
}
