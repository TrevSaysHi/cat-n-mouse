﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PastorPurrsian : MonoBehaviour {

	enum Actions { Idle = 0, MoveLeft = 1, MoveRight = 2, Reading = 3, ReadingWithMuffs = 4, Blessing = 5, MoveLeftWithMuffs = 6, MoveRightWithMuffs = 7, IdleWithMuffs = 8 }

	Actions[] baseActions = { Actions.Idle, Actions.MoveLeft, Actions.MoveRight, Actions.Reading };
	Actions[] baseActionsWithMuffs = { Actions.IdleWithMuffs, Actions.MoveLeftWithMuffs, Actions.MoveRightWithMuffs, Actions.ReadingWithMuffs, Actions.Blessing };

	const int DEFAULT_CHARACTER_LAYER = 9;
	const int IN_FRONT_OF_BUILDING_CHARACTER_LAYER = 13;
	const int DEFAULT_ROOM_LAYER = 8;

	static Vector3 outsideRoom = new Vector3 (7f, -3.61f, 0f);
	static Vector3 insideRoom = new Vector3 (0f, -3.61f, 0f);

	Transitions[] toOutsideRoom = {
		new Transitions (outsideRoom, 5f, DEFAULT_CHARACTER_LAYER - 5, DEFAULT_ROOM_LAYER - 5, 1)
	};
	Transitions[] toInsideRoom = {
		new Transitions (insideRoom, 5f, DEFAULT_CHARACTER_LAYER - 5, DEFAULT_ROOM_LAYER - 5, 5)
	};

	Animator animator;
	SpriteRenderer spriteRenderer, roomLayer;
	TimeController timeController;

	private IEnumerator roomTransition;

	public float leftRoomBoundary;
	public float rightRoomBoundary;
	public float speed;
	public int start = 0;
	public int leavesToGetHeadphones = 160;
	public int comesBackWithCross = 175;
	public int readsWithHeadphones = 180;
	public int sleeps = 200;
	public int end = 300;
	private int currentAction;
	private int randomDecisionTime;
	private int nextActionTime;

	const string LOCATION_INSIDE = "insideRoom";
	const string LOCATION_INSIDE_WITH_MUFFS = "insideRoomWMuffs";

	private bool canStartCoRoutine = true;
	private bool wearingMuffs = false;

	// Use this for initialization
	void Start () {
		animator = GetComponent<Animator> ();
		roomLayer = GameObject.Find ("PastorPersians Room").GetComponent<SpriteRenderer> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		timeController = GameObject.Find ("Clock").GetComponent<TimeController> ();

		setLayers (DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER);

		ChooseNextAction (baseActions, 2.0f, 4.0f);
	}

	// Update is called once per frame
	void Update () {
		if (CheckIfInTime (start, leavesToGetHeadphones - 1)) {
			DetermineActionFromLocation (LOCATION_INSIDE);
			CheckIfOutOfBounds (leftRoomBoundary, rightRoomBoundary);
		}
		if (CheckIfInTime (leavesToGetHeadphones, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toOutsideRoom);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
		}
		if (CheckIfInTime (leavesToGetHeadphones + 5, comesBackWithCross - 5)) {
			animator.SetInteger ("State", 0);
			canStartCoRoutine = true;
		}
		if (CheckIfInTime (comesBackWithCross, 0)) {
			if (canStartCoRoutine) {
				roomTransition = RoomTransition (toInsideRoom);
				StartCoroutine (roomTransition);
				canStartCoRoutine = false;
			}
			wearingMuffs = true;
		}
		if (CheckIfInTime (readsWithHeadphones, sleeps)) {
			DetermineActionFromLocation (LOCATION_INSIDE_WITH_MUFFS);
			CheckIfOutOfBounds (leftRoomBoundary, rightRoomBoundary);
			setLayers (DEFAULT_CHARACTER_LAYER, DEFAULT_ROOM_LAYER);
		}
		if (CheckIfInTime (sleeps, end)) {
			animator.SetInteger ("State", 2);
		}
	}

	void DetermineActionFromLocation (string location) {
		if (timeController.timeInSeconds == nextActionTime) {
			if (location.Equals (LOCATION_INSIDE)) {
				ChooseNextAction (baseActions, 2.0f, 4.0f);
			} else if (location.Equals (LOCATION_INSIDE_WITH_MUFFS)) {
				ChooseNextAction (baseActionsWithMuffs, 2.0f, 8.0f);
			}
		}
		SetAction ();
	}

	void ChooseNextAction (Actions[] actionList, float minDecisionTime, float maxDecisionTime) {
		int nextAction = Mathf.RoundToInt (Random.Range (0.0f, actionList.Length - 1));
		currentAction = (int) actionList[nextAction];

		// Debug.Log("Current Action for " + this.gameObject.name  + ": " + currentAction);

		randomDecisionTime = Mathf.RoundToInt (Random.Range (minDecisionTime, maxDecisionTime));
		nextActionTime = timeController.timeInSeconds + randomDecisionTime;
	}

	void SetAction () {
		if (currentAction == (int) Actions.Idle) {
			animator.SetInteger ("State", 0);
			transform.position = transform.position;
		} else if (currentAction == (int) Actions.MoveLeft) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.left * Time.deltaTime * speed;
			SetXScale (-.5f);
		} else if (currentAction == (int) Actions.MoveRight) {
			animator.SetInteger ("State", 1);
			transform.position += Vector3.right * Time.deltaTime * speed;
			SetXScale (.5f);
		} else if (currentAction == (int) Actions.MoveLeftWithMuffs) {
			animator.SetInteger ("State", 8);
			transform.position += Vector3.left * Time.deltaTime * speed;
			SetXScale (-.5f);
		} else if (currentAction == (int) Actions.MoveRightWithMuffs) {
			animator.SetInteger ("State", 8);
			transform.position += Vector3.right * Time.deltaTime * speed;
			SetXScale (.5f);
		} else if (currentAction == (int) Actions.ReadingWithMuffs) {
			animator.SetInteger ("State", 3);
		} else if (currentAction == (int) Actions.Blessing) {
			animator.SetInteger ("State", 6);
		} else if (currentAction == (int) Actions.IdleWithMuffs) {
			animator.SetInteger ("State", 7);
		} else if (currentAction == (int) Actions.Reading) {
			animator.SetInteger ("State", 4);
		}
	}

	void CheckIfOutOfBounds (float leftBoundary, float rightBoundary) {
		if (!wearingMuffs) {
			if (transform.position.x < leftBoundary) {
				currentAction = (int) Actions.MoveRight;
				SetAction ();
			}
			if (transform.position.x > rightBoundary) {
				currentAction = (int) Actions.MoveLeft;
				SetAction ();
			}
		} else {
			if (transform.position.x < leftBoundary) {
				currentAction = (int) Actions.MoveRightWithMuffs;
				SetAction ();
			}
			if (transform.position.x > rightBoundary) {
				currentAction = (int) Actions.MoveLeftWithMuffs;
				SetAction ();
			}
		}
	}

	bool CheckIfInTime (int beginningTime, int endTime) {
		if (endTime == 0 && timeController.timeInSeconds == beginningTime) {
			return true;
		}

		if (beginningTime < timeController.timeInSeconds && timeController.timeInSeconds < endTime)
			return true;
		else
			return false;
	}

	void SetXScale (float scale) {
		transform.localScale = new Vector3 (scale, transform.localScale.y, transform.localScale.z);
	}

	void setLayers (int newCharLayer, int newRoomLayer) {
		spriteRenderer.sortingOrder = newCharLayer;
		roomLayer.sortingOrder = newRoomLayer;
	}

	private IEnumerator RoomTransition (Transitions[] positionsToMoveTo) {
		float elapsedTime;
		Vector3 startPos;

		for (int i = 0; i < positionsToMoveTo.Length; i++) {
			elapsedTime = 0f;
			startPos = transform.position;

			if (transform.position.x < positionsToMoveTo[i].position.x) {
				SetXScale (.5f);
			} else {
				SetXScale (-.5f);
			}

			setLayers (positionsToMoveTo[i].characterLayer, positionsToMoveTo[i].roomLayer);
			animator.SetInteger ("State", positionsToMoveTo[i].characterAnimationState);

			while (elapsedTime < positionsToMoveTo[i].transitionTime) {
				transform.position = Vector3.Lerp (startPos, positionsToMoveTo[i].position, elapsedTime / positionsToMoveTo[i].transitionTime);
				elapsedTime += Time.deltaTime;
				yield return null;
			}
		}

		animator.SetInteger ("State", 0);
		nextActionTime = timeController.timeInSeconds + 1;
		StopCoroutine ("RoomTransition");
	}
}